#!/usr/bin/env python

""" Logging middleware """

# source: https://codegists.com/code/python-falcon-logging/

__author__      = "morientes"

import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.FileHandler('octopus.log'))
logger.setLevel(logging.INFO)


class ResponseLoggerMiddleware(object):

	def process_response(self, req, resp, url):
		logger.info('{0} {1} {2}'.format(req.method, req.relative_uri, resp.status[:3]))