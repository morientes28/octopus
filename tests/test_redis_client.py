#!/usr/bin/env python

""" Unit test of redis client """

__author__      = "morientes"

import redis
import falcon
import falcon.testing as testing
from redis_client import RedisClient


class TestRedisClient(testing.TestCase):
	""" testing redis functionality wrapped by redis client """ 

	rc = RedisClient(host='localhost', port=6379)

	def setUp(self):
		self.rc.set("test", 123) # redis convert integer to string
		self.rc.set("test2", "abcd")

	def test_get(self):		
		self.assertNotEqual(self.rc.get("test"), 123)
		self.assertEqual(self.rc.get("test"), "123")
		self.assertEqual(self.rc.get("test2"), "abcd")
