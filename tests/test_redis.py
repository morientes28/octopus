#!/usr/bin/env python

""" Unit test of redis server """

__author__      = "morientes"

import redis
import falcon
import falcon.testing as testing

class TestRedis(testing.TestCase):

	pool = redis.ConnectionPool(host='localhost', port=6379, db=0)

	def setUp(self):
		self.redis = redis.Redis(connection_pool=self.pool)

	def test_connect_to_redis(self):
		r = redis.Redis(connection_pool=self.pool)
		self.assertNotEqual(r, None)

	def test_set_get_value(self):
		r = redis.Redis(connection_pool=self.pool)
		r.set('foo', 'bar')
		value = r.get('foo')
		self.assertEqual(value, 'bar')
