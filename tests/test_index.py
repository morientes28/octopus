#!/usr/bin/env python

""" Unit test of index controler """

__author__      = "morientes"

import falcon.testing as testing
from index import LoadResource, SaveResource, run_falcon_app, CreateTokenResource, ValidateTokenResource
from redis_client import RedisClient


class MockRedisClient:

	def __init__(self):
		self.key = "1234"
		self.value = "abcd"

	def get(self, key):
		if key == self.key:
			return self.value
		return None

	def set(self, key, value, expire=0):
		self.key = key
		self.value = value
		return self.value


class TestIndex(testing.TestCase):
	""" testing basic mapping """

	def setUp(self):
		self.api = run_falcon_app()
		self.mock_rc = MockRedisClient()
		self.rc = RedisClient()
		self.resource = LoadResource(self.mock_rc)
		self.resource2 = SaveResource(self.mock_rc)

		self.api.add_route('/{url}', self.resource)
		self.api.add_route('/', self.resource2)
		self.api.add_route('/sts/', CreateTokenResource(self.rc))
		self.api.add_route('/sts/{token}', ValidateTokenResource(self.rc))

	def test_get_root(self):
		result = self.simulate_get('/')
		self.assertEqual(result.status_code, 200)

	def test_post_save_route(self):
		result = self.simulate_post('/')
		self.assertEqual(result.status_code, 400) # except HTTPInvalidParam, missing url

	def test_get_url_in_db_mock(self):
		result = self.simulate_get('/1234')
		self.assertEqual(result.status_code, 307)

	def test_get_url_not_in_db_mock(self):
		result = self.simulate_get('/notInDB')
		self.assertEqual(result.status_code, 200)
		self.assertEqual(result.content, LoadResource.NOT_IN_DB)

	def test_post_save_url_mock(self):
		qs = 'url=1234'
		result = self.simulate_request('POST', '/', body=qs)
		self.assertEqual(result.status_code, 200)
		self.assertEqual(result.content, self.mock_rc.get('1234'))

	def test_save_and_get_url_in_db(self):
		self.resource = LoadResource(self.rc)
		self.resource2 = SaveResource(self.rc)
		qs = 'url=AAAx'
		result = self.simulate_request('POST', '/', body=qs)
		value = result.content
		self.assertEqual(result.status_code, 200)

		result = self.simulate_request('GET', '/' + value)
		value = result.content
		print(value)
		#self.assertEqual(result.status_code, 307)

	def test_get_token(self):
		result = self.simulate_get('/sts/')
		self.assertEqual(result.status_code, 200)

	def test_set_token(self):
		qs = 'in=aaaaaaaa&type=text'
		result = self.simulate_request('POST', '/sts/', body=qs)
		value = result.content
		print(value)
		self.assertEqual(result.status_code, 200)

		result = self.simulate_request('GET', '/sts/' + value)
		value = result.content
		self.assertEqual(result.status_code, 200)

