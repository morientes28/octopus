#!/usr/bin/env python

""" Lightweight Redis client """

__author__      = "morientes"

import redis
from redis.exceptions import ConnectionError, RedisError

class RedisClient:

	r = None

	def __init__(self, host='localhost', port=6379, from_url=None):
		if from_url:
			self.r = redis.from_url(from_url)
		else:
			self.pool = redis.ConnectionPool(host=host, port=port, db=0)
		self.connection_error_message = "Connection to redis failed, check if redis is enabled"

	def set(self, key, value, expire=0):
		if not self.r:
			try:
				self.r = redis.Redis(connection_pool=self.pool)
				if expire <= 0:
					expire = None
			except ConnectionError:
				print(self.connection_error_message)
		self.r.set(key, value, ex=expire)

	def get(self, key):
		if not self.r:
			try:
				self.r = redis.Redis(connection_pool=self.pool)
			except ConnectionError:
				print(self.connection_error_message)
		return self.r.get(key)

	def delete(self, key):
		if not self.r:
			try:
				self.r = redis.Redis(connection_pool=self.pool)
			except ConnectionError:
				print(self.connection_error_message)
		return self.r.delete(key)