#!/bin/bash

rs=$(pidof redis-server)
if [[ -z ${rs} ]]; then
  echo "redis server is not running."
  echo "start redis server ..."
  nohup redis-server &
  sleep 1
  rs=$(pidof redis-server)
fi

echo "OK - redis server is running with PID: ${rs}"

gunicorn index:app