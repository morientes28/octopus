# README #

Secure token service

### What is this repository for? ###

1. Primary purpose : generate secure token for authorizations
2. Secondary purpose : generate short url


### Technologies ###

* python framework falcon (https://falconframework.org/)
* Redis db (http://redis.io/topics/quickstart)


### Url mapping ###

/      - form for insert url
/(url) - mapping url

sts/   - form for creating token
str/(token) - validate token


### Funcionality ###

STS - service for creating and validating token. You can generate token for once or more uses.
Default token has unlimited time expiration, but you can set expiration time.


### Version ###

0.0.3 

+ custom url in short url funcionality

0.0.2

+ new bootsrap design

0.0.1

+ initial prototype


### TODO ###

- new custom url can not rewrite old, add check
- documented REST API
- AUTH for REST API (has already prepared)
- short link with password
- tracking clicked link
- project page