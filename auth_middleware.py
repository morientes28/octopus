#!/usr/bin/env python

""" Authorization middleware """

# source: https://github.com/falconry/falcon

__author__      = "morientes"

import falcon

class AuthMiddleware(object):

    def __init__(self, enabled=True):
      self.enabled_authorization = enabled;

    def process_request(self, req, resp):

        if self.enabled_authorization:

          token = req.get_header('Authorization')
          account_id = req.get_header('Account-ID')

          challenges = ['Token type="Fernet"']

          if token is None:
              description = ('Please provide an auth token '
                             'as part of the request.')

              raise falcon.HTTPUnauthorized('Auth token required',
                                            description,
                                            challenges,
                                            href='http://docs.example.com/auth')

          if not self._token_is_valid(token, account_id):
              description = ('The provided auth token is not valid. '
                             'Please request a new token and try again.')

              raise falcon.HTTPUnauthorized('Authentication required',
                                            description,
                                            challenges,
                                            href='http://docs.example.com/auth')
   
    def _token_is_valid(self, token, account_id):
        return True  # Suuuuuure it's valid...

    @staticmethod
    def _token_user_is_valid(token, account_id):
        return True  # Suuuuuure it's valid...

    @staticmethod
    def autorization(req, resp, resource, params):
      LINK_TO_HELP = 'http://docs.example.com/auth'
      token_type = ['Token type="Fernet"']
      token = req.get_header('Authorization')
      account_id = req.get_header('Account-ID')
      if token is None:
        description = ('Please provide an auth token as part of the request.')

        raise falcon.HTTPUnauthorized('Auth token required',
                        description,
                        token_type,
                        href=LINK_TO_HELP)

      if not AuthMiddleware._token_user_is_valid(token, account_id):
        description = ('The provided auth token is not valid. '
                'Please request a new token and try again.')

        raise falcon.HTTPUnauthorized('Authentication required',
                          description,
                          token_type,
                          href=LINK_TO_HELP)



#@falcon.before(max_body(64 * 1024))  # anotovat do_post metodu ak chcem pouzit hook
#def max_body(limit):

#    def hook(req, resp, resource, params):
#        length = req.content_length
#        if length is not None and length > limit:
#            msg = ('The size of the request is too large. The body must not '
#                   'exceed ' + str(limit) + ' bytes in length.')

#            raise falcon.HTTPRequestEntityTooLarge(
#                'Request body is too large', msg)

#    return hook