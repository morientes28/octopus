#!/usr/bin/env python

""" Base controller index for shorting url """
__author__ = "morientes"

import falcon
import os
from falcon.util import uri
import random
import string
import logging_middleware
from auth_middleware import AuthMiddleware
from redis_client import RedisClient


class BaseResource(object):
	""" Base class with common methods """

	USE_ONLY_ONCE_FLAG = 'u4o'
	NOT_IN_DB = 'Token is not in DB'

	def __init__(self, redis_client):
		self.client = redis_client

	def do_get(self, req, resp, url=None):
		#req.set_header('Authorization', 'asdf')
		#req.set_header('Account-ID', '123')
		pass

	def _save_url(self, url, custom=None, length_token=6, expire=0, use_once=False):
		if custom:
			store_url = self.client.get(custom)
		else:
			store_url = self.client.get(url)

		if store_url is None:
			gen_string = ''.join(
				[random.choice(string.ascii_letters + string.digits) for n in xrange(length_token)])
			if use_once:
				gen_string = self.USE_ONLY_ONCE_FLAG + gen_string[0:length_token-3]

			if custom:
				gen_string = custom	

			self.client.set(gen_string, url, expire=int(expire) * 60)
			self.client.set(url, gen_string, expire=int(expire) * 60)  # Revers save too
			return gen_string
		return store_url


class LoadResource(BaseResource):
	""" Handle GET requests for load mapped URL """

	def on_get(self, req, resp, url):
		super(LoadResource, self).do_get(req, resp, url)
		if self._validate(url):
			store_url = self.client.get(url)
			if store_url is not None:
				resp.status = falcon.HTTP_307
				raise falcon.HTTPTemporaryRedirect(store_url)
			else:
				resp.status = falcon.HTTP_200
				resp.body = self.NOT_IN_DB
		else:
			resp.status = falcon.HTTP_500
			raise ValueError("bad request")

	@staticmethod
	def _validate(url):
		if url is None:
			return False
		return True


class SaveResource(BaseResource):
	""" Handle GET requests for load mapped URL
		Handle POST request for save URL
	"""

	def on_get(self, req, resp):
		super(SaveResource, self).do_get(req, resp)
		resp.status = falcon.HTTP_200	
		resp.content_type = 'text/html'
		with open('templates/index.html', 'r') as f:
			resp.body = f.read()

	def on_post(self, req, resp):
		body = req.stream.read()
		if self._validate(body):
			extra_params = uri.parse_query_string(body.decode('utf-8'),)
			#extra_params = uri.parse_query_string(uri.decode(body.decode('utf-8')),)
			if 'url' in extra_params:
				url = extra_params['url']
				custom = None
				if 'custom' in extra_params:
					custom = extra_params['custom']
				gen_string = self._save_url(url, custom)
				resp.status = falcon.HTTP_200  # This is the default status
				resp.body = gen_string
				return

		raise falcon.HTTPInvalidParam("bad request", "url")

	@staticmethod
	def _validate(body):
		if body is None:
			return False
		return True


class CreateTokenResource(BaseResource):
	""" Handles GET and POST requests
		for security token service
	"""

	def on_get(self, req, resp):
		super(CreateTokenResource, self).do_get(req, resp)
		resp.status = falcon.HTTP_200
		resp.content_type = 'text/html'
		with open('templates/sts.html', 'r') as f:
			resp.body = f.read()

	def on_post(self, req, resp):
		""" Handles POST requests """
		body = req.stream.read()
		params = self._parse_request(body)
		gen_string = self._save_url(
								params['in'],
								32,
								params['time_valid'],
								params['use_valid'])

		resp.status = falcon.HTTP_200  # This is the default status
		resp.body = gen_string

	@staticmethod
	def _validate(body):
		if body is None:
			return False			
		return True

	@staticmethod
	def _parse_request(body):
		if body is None:
			raise falcon.HTTPInvalidParam("bad request", body)

		params = uri.parse_query_string(body.decode('utf-8'),)
		if 'in' in params:
			inp = params['in']
			if 'time_valid' not in params:
				params['time_valid'] = 0
			if 'use_valid' not in params:
				params['use_valid'] = False
			else:
				params['use_valid'] = True
			return params
		else:
			raise falcon.HTTPInvalidParam("bad request, missing parameter [in]", body)
	

class ValidateTokenResource(BaseResource):
	""" Handle GET requests for validate token """

	#@falcon.before(AuthMiddleware.autorization)
	def on_get(self, req, resp, token):
		super(ValidateTokenResource, self).do_get(req, resp, token)
		if self._validate(token):
			store_token = self.client.get(token)
			if store_token is not None:
				if token.startswith(self.USE_ONLY_ONCE_FLAG):
					self.client.delete(token)
				resp.status = falcon.HTTP_200
				resp.body = store_token
			else:
				resp.status = falcon.HTTP_200
				resp.body = self.NOT_IN_DB
		else:
			resp.status = falcon.HTTP_500
			raise ValueError("bad request")

	@staticmethod
	def _validate(token):
		if token is None:
			return False
		return True
		
class InfoResource():
	""" Handles GET requests
		for static info html
	"""

	def on_get(self, req, resp):
		resp.status = falcon.HTTP_200
		resp.content_type = 'text/html'
		with open('templates/info.html', 'r') as f:
			resp.body = f.read()

# ------------------------------------------------------------------------------------

def run_falcon_app():
	# falcon.API instances are callable WSGI apps
	# added logging middleware for logging request
	app = falcon.API(middleware=[logging_middleware.ResponseLoggerMiddleware(),
								 AuthMiddleware(False)]
								 )

	# one instance of redis client
	from_url = "redis://redistogo:18dbf939e801900aad87f1692989dfae@porgy.redistogo.com:9854"
	redis_client = RedisClient(from_url=from_url)

	# handling request
	app.add_route('/', SaveResource(redis_client))
	app.add_route('/{url}', LoadResource(redis_client))

	app.add_route('/sts/', CreateTokenResource(redis_client))
	app.add_route('/sts/{token}', ValidateTokenResource(redis_client))

	app.add_route('/info', InfoResource())

	return app


app = run_falcon_app()
